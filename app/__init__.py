from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)
app.config.from_object('app.config.DevelopmentConfig')

db = SQLAlchemy(app)
ma = Marshmallow(app)

@app.route('/')
def index():
  return 'Добро пожаловать на главную страницу мега социальной сети!'
