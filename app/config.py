class BaseConfig:
  DEBUG = False
  TESTING = False

class ProductionConfig(BaseConfig):
  DEBUG = False

class StagingConfig(BaseConfig):
  DEVELOPMENT = True
  DEBUG = True

class DevelopmentConfig(BaseConfig):
  DEVELOPMENT = True
  DEBUG = True

class TestingConfig(BaseConfig):
  TESTING = True
